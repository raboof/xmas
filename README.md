# xmas

Festive outside lighting!

## hardware

And atmega256 on a breadboard with the 'making the shrimp'
components to make it act like an arduino, and a neopixel
ring attached:

![board](./breadboard.png)

Put inside our outside light, with some crumpled-up plastic
bread bags as a makeshift diffuser:

![video](./video.mp4)

## software

pulsating red and green light at different intervals, and
'rotating' which halve of the ring is red and which halve is
green. This gave a nicer effect 
